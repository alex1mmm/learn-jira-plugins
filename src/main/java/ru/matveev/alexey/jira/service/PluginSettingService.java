package ru.matveev.alexey.jira.service;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class PluginSettingService {

    private final PluginSettings pluginSettings;
    private final String PLUGIN_STORAGE_KEY = "ru.matveev.alexey.task1";

    @Inject
    public PluginSettingService(@ComponentImport PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();

    }

    public String getParameter1() {
        return this.getParameter("parameter1");  }

    public String getParameter2() {
        return this.getParameter("parameter2");
    }

    public void setParameter1(String parameter1) {
       this.setParameter("parameter1", parameter1);
    }

    public void setParameter2(String parameter2) {
        this.setParameter("parameter2", parameter2);
    }

    public String getParameter(String parameterName) {
        return pluginSettings.get(PLUGIN_STORAGE_KEY + parameterName) == null ||  pluginSettings.get(PLUGIN_STORAGE_KEY + parameterName).toString().isEmpty()  ? "no value" : pluginSettings.get(PLUGIN_STORAGE_KEY + parameterName).toString();
    }

    public void setParameter(String parameterName, String parameterValue) {
        this.pluginSettings.put(PLUGIN_STORAGE_KEY + parameterName, parameterValue);
    }
}

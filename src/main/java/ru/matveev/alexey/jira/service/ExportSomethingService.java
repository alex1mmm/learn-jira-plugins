package ru.matveev.alexey.jira.service;

import ru.matveev.alexey.jira.service.PluginSettingService;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ExportSomethingService {
    private final PluginSettingService pluginSettingService;

    @Inject
    public ExportSomethingService(PluginSettingService pluginSettingService) {
        this.pluginSettingService = pluginSettingService;
    }

    public void exportDate() {
        this.pluginSettingService.getParameter1();

    }
}

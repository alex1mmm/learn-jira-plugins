$(function () {

AJS.formValidation.register(['positiveinteger'], function(field) {
    fieldValue = field.el.value.replace(/^0*/, "");
    var n = Math.floor(Number(fieldValue));
        if (!(n !== Infinity && String(n) === fieldValue && n > 0)) {
          field.invalidate(AJS.format('Input must be positive integer'));
        }
        field.validate();
});
})